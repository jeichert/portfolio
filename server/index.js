import path from 'path';
import express from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import App from './generated/app';

const app = express();
const PORT = 3000;

// View Engine Config
app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

// Static Assets
app.use(express.static(path.resolve(__dirname, '../dist')));

// Routes
app.get('/', (req, res) => {
  res.render('index', {
    app: ReactDOMServer.renderToString(<App />)
  });
});


app.listen(PORT, () => console.log(`Server running on localhost:${PORT}`));
