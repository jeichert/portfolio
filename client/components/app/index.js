import React, {Component} from 'react';
import './style.scss';

class App extends React.Component {
  render() {
    return <div>Hello from a react component</div>;
  }
}

export default App;
